#include <gtest/gtest.h>
#include "bubble_sort.hpp"

TEST(BubbleSortTest, EmptyVector) {
    std::vector<int> arr;
    bubbleSort(arr);
    ASSERT_TRUE(arr.empty());
}

TEST(BubbleSortTest, SortedVector) {
    std::vector<int> arr = {1, 2, 3, 4, 5};
    bubbleSort(arr);
    for (size_t i = 0; i < arr.size(); ++i) {
        EXPECT_EQ(arr[i], i + 1);
    }
}

TEST(BubbleSortTest, ReverseSortedVector) {
    std::vector<int> arr = {5, 4, 3, 2, 1};
    bubbleSort(arr);
    for (size_t i = 0; i < arr.size(); ++i) {
        EXPECT_EQ(arr[i], i + 1);
    }
}

TEST(BubbleSortTest, RandomVector) {
    std::vector<int> arr = {3, 1, 4, 1, 5, 9, 2, 6, 5};
    std::vector<int> sortedArr = {1, 1, 2, 3, 4, 5, 5, 6, 9};
    bubbleSort(arr);
    EXPECT_EQ(arr, sortedArr);
}
